# TIENDEO Node.js code assessment

Program that calculates the drones destination based on a list of commands, including original location and maneuvers


## Project setup
```bash
	npm install
	npm run build
```

## Run program
```bash
	npm start
```

## Run tests
```bash
	npm test
```

## Additional comments
Implemented using Typescript, object oriented paradigm for models, functional programming for general functionalities, state pattern for drone location and singleton for flight area.

The commands could be sended by console or by text file and I have chosen the latter one.

Several implementations were possible but I have tried to avoid the ones that can be found easily (f.e. Mars Rover and so on)
