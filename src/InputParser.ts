import { EOL } from 'os';

const processInput = (inputCommands: Buffer) => {
  const input = inputCommands.toString()

  console.log(`Input:\n${input}\n`)

  const commands = input.split(EOL)
  const area = commands.shift()

  return { area, commands }
}

export default {
  processInput
}