import DronesMovement from './services/DronesMovement'
import Drone from './models/Drone'
import DroneFactory from './factories/DroneFactory'
import Location from './models/Location'

const { changeOrientation, changePosition } = DronesMovement

const outputWriter = (commands: string[], drones: Drone[], flightArea: any) => {
  console.log('Output:')

  let drone: Drone, initX: number, initY: number, initOrientation: string

  commands.forEach((command: string, line: number) => {
    switch (line%2) {
      case 0:
        const [x, y, orientation] = [...command]
        initX = Number(x)
        initY = Number(y)
        initOrientation = orientation

        drone = DroneFactory.create(initX, initY, orientation, flightArea)
        drones.push(drone)
        break
      case 1:
        const directions = [...command]

        drone.move(directions, changeOrientation, changePosition)
        drone.printLocation()
        drone.moveToLocation(new Location(initX, initY, initOrientation))
    } 
  })
}

export default { outputWriter }