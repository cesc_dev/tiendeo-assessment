import Area from '../models/Area'

class AreaFactory {
  public static create(area: string): any {
    const [length, width] = area.split('')
  
    return Area(Number(length), Number(width))
  }
}

export default AreaFactory;