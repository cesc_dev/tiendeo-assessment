import Drone from '../models/Drone'

class DroneFactory {
  public static create(x: number, y: number, orientation: string, area: any): Drone {
    return new Drone(x, y, orientation, area)
  }
}

export default DroneFactory;