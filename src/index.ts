import { readFile } from "fs";
import { promisify } from "util";
import { join } from 'path';
import Drone from './models/Drone'
import AreaFactory from "./factories/AreaFactory";
import InputParser from './InputParser'
import OutputWriter from './OutputWriter'

const { processInput } = InputParser
const { outputWriter } = OutputWriter
let drones: Drone[] = []

promisify(readFile)(join(__dirname, '..', 'data', 'commands.txt'))
  .then((contents) => {
    const { area, commands } = processInput(contents)
    let flightArea: any

    if (area) {
      flightArea = AreaFactory.create(area)
    }

    outputWriter(commands, drones, flightArea)
  })
  