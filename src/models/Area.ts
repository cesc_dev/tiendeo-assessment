class Area {
  length: number
  width: number

  constructor(length: number, width: number) {
    this.length = length
    this.width = width
  }
}

export default (length: number, width: number) => new Area(length, width)