import Location from './Location'

class Drone {
  currentState: string | null
  states: any
  flightArea: any

  constructor(x: number, y: number, orientation: string, area: any) {
    this.currentState = null
    this.states = {
      still: new Location(x, y, orientation),
      move: null,
    }
    this.changeState('still')
    this.flightArea = area
  }

  changeState(state: string) {
    this.currentState = this.states[state]
  }

  move(directions: string[], changeOrientation: any, changePosition: any, ): void {
    const direction: string | undefined = directions.shift()

    if (direction) {
      if (!this.states.move) {
        this.states.move = this.states.still
      }
      
      const { x: initX, y: initY, orientation: initOrientation } = this.states.move
      const newOrientation = changeOrientation[direction][initOrientation]
      let { x, y } = changePosition[direction](initX, initY, newOrientation)
      const newX = x > this.flightArea.length ? x = this.flightArea.length : x
      const newY = y > this.flightArea.width ? y = this.flightArea.width : y
  
      this.states.move = new Location(newX, newY, newOrientation)
      this.changeState('move')

      return this.move(directions, changeOrientation, changePosition)
    }
  }

  moveToLocation(location: Location) {
    this.states.move = location
    this.changeState('move')
  }

  printLocation(): void {
    let destination = this.states.move

    console.log(`${destination.x}${destination.y}${destination.orientation}`)
  }
}

export default Drone