class Location {
  x: number
  y: number
  orientation: string

  constructor(x: number, y: number, orientation: string) {
    this.x = x
    this.y = y
    this.orientation = orientation
  }
}
export default Location