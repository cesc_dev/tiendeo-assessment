import { Orientations } from '../types/types'

const { N, S, E, W } = Orientations

const changeOrientation = {
  L: {
    N: W,
    S: E,
    E: N,
    O: S
  },
  R: {
    N: E,
    S: W,
    E: S,
    O: N
  },
  M: {
    N: N,
    S: S,
    E: E,
    O: W
  },
}

const changePosition = {
  L: (x: number, y: number) => ({ x, y }),
  R: (x: number, y: number) => ({ x, y }),
  M: (x: number, y: number, orientation: string) => {
    switch(orientation) {
      case N:
        y++
        break
      case S:
        y--
        break
      case E:
        x++
        break
      case W:
        x--
        break
    }
    return { x, y }
  },
}

export default {
  changeOrientation,
  changePosition,
}