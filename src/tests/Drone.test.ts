import Drone from '..//models/Drone'
import Area from '../models/Area'
import Location from '../models/Location'
import DronesMovement from '../services/DronesMovement'

const { changeOrientation, changePosition } = DronesMovement

describe('drone movement', () => {
  const area = Area(5, 5)

  test('when not flying the drone state should be its initial location', () => {
    const drone = new Drone(1, 1, 'E', area)
    const initLocation = new Location(1, 1, 'E')
    const initialState = drone.currentState
    expect(initialState).toStrictEqual(initLocation)
  })

  test('when flying the drone state should be its current location', () => {
    const drone = new Drone(1, 1, 'E', area)
    drone.move(['M'], changeOrientation, changePosition)
    const newLocation = new Location(2, 1, 'E')
    const lastState = drone.currentState
    expect(lastState).toStrictEqual(newLocation)
  })

  test('when the drone is at the area limit it should not go forward', () => {
    const drone = new Drone(5, 1, 'E', area)
    drone.move(['M'], changeOrientation, changePosition)
    expect(drone.states.still).toStrictEqual(drone.states.move)
  })

  test('when flying and following directions it should end in the expected location', () => {
    const drone = new Drone(1, 1, 'E', area)
    drone.move(['M', 'M', 'R'], changeOrientation, changePosition)
    const expectedLocation = new Location(3, 1, 'S')
    expect(drone.currentState).toStrictEqual(expectedLocation)
  })

  test('when flying back to the base it should end in the initial location', () => {
    const drone = new Drone(1, 1, 'E', area)
    const expectedLocation = new Location(1, 1, 'E')
    drone.move(['M', 'M', 'R'], changeOrientation, changePosition)
    drone.moveToLocation(expectedLocation)
    expect(drone.currentState).toStrictEqual(expectedLocation)
  })
})