export enum Orientations {
  N = 'N',
  S = 'S',
  E = 'E',
  W = 'O'
}

export enum Directions {
  L = 'L',
  R = 'R',
  M = 'M'
}